package com.srini.kafkaconfluentproducer1;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

/**
 * The type Kafka confluent producer 1 application.
 */
@SpringBootApplication
@EnableKafka
@RequiredArgsConstructor
public class KafkaConfluentProducer1Application implements CommandLineRunner {

	private final Producer producer ;

	/**
	 * The entry point of application.
	 *
	 * @param args the input arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(KafkaConfluentProducer1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		producer.sendStocks();
	}
}
